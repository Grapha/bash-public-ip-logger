# [Bash] Public IP Logger

**Version:** 0.5.0-stable


---

**Sommaire :**

[[_TOC_]]

---


## 1. Présentation

Script en **Bash** permettant de **journaliser les changements d'IP publique** que le Fournisseur d'Accès Internet (FAI)
fournit à l'interface de connexion Internet courante de la machine.


### Motivation

Ce projet à vu le jour pour détecter l'intervalle de rafraichissement de l'IP publique fournit par le FAI d'un abonnement mobile.
*Car un changement d'IP a la fâcheuse tendance à interrompre les transferts de donnée en cours qui s'exécuteraient durant cet intervalle.*

*Pour info, lorsque l'IP est allouée dynamiquement (dans la plupart des cas à l'exception de certain serveur), alors,
une réinitialisation du terminal aura pour effet de renégocier un nouveau bail et donc une nouvelle IP publique
(cf. ON/OFF/ON de la box, ou mode avion du téléphone pendant quelques secondes).*


### État du projet

Le module est utilisable dans sa version **0.5.0** avec le status de **stable**.

L'étiquette *'latest'* renvoie à cette dernière version.


### Licence

**GNU General Public License version 3 or latter**
([html](https://www.gnu.org/licenses/gpl-3.0.html)
, [txt](https://www.gnu.org/licenses/gpl-3.0.txt)
, [md](https://www.gnu.org/licenses/gpl.md))
, SPDX: [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html).

Une copie est disponible dans le fichier [license.md](license.md).


### Structure du projet

 - **license.md** :                           Licence GNU GPL v3.
 - **readme.md** :      		              Documentation de départ.
 - **script--log-public-ips-changes.sh** :    Script journalisant les changements d'IP publique.


### Fonctionnalités fournies

 - Récupère l'IP publique par une requête HTTP à **ifconfig.me** via **curl**.
 - Affiche la date, l'heure et l'IP publique actuelle sur la sortie standard (i.e. *stdout*).
 - Journalise en **JSON** l'IP, le timestamp et la date.
 - Actualise en continu l'IP selon une temporisation pour vérifier les éventuels changements. 
 - Affiche seulement les changements (*VERBOSE=0*), ou affiche toutes les actualisations (*VERBOSE=1*).
 - Désactivation de l'affichage sur *stdout* possible (*QUIET=1*).

Pour les détails, se référer aux sources.

*Note : Testé seulement sous Debian 10 x64.*


---


## 2. Installation

### 2.1. Dépendances

#### bash

```bash
$ bash --version
GNU bash, version 5.1.4(1)-release (x86_64-pc-linux-gnu)
```

Normalement déjà présent sur pratiquement toutes les distributions GNU/Linux.

#### curl

Le paquet **curl** est nécessaire pour avoir accès à la commande **curl**.
Sur beaucoup de distribution GNU/Linux, ce paquet est présent par défaut, et ne nécessite donc pas d'installation.

```bash
$ curl --version
curl 7.74.0 <...>
```

#### python3

**Python 3** est nécessaire pour avoir accès au module **random** (présent dans la distribution standard de Python).
Python aussi est très souvent présent par défaut sur beaucoup de distribution GNU/Linux. 

```bash
# apt install python3
$ python3 --version
Python 3.9.2
```

### 2.2. Le script

On clone le **Git** et on donne les **droits d'exécution** au script.

```bash
$ git clone https://framagit.org/Grapha/bash-public-ip-logger.git
$ cd bash-public-ip-logger/
$ chmod u+x script--log-public-ips-changes.sh
```

---


## 3. Utilisation

L'utilisation du script se fait classiquement :

```bash
$ ./script--log-public-ips-changes.sh [-h | --help]
$ ./script--log-public-ips-changes.sh [REFRESH_DELAY [RANDOM_DELAY]]
```

Il faudra arrêter le script manuellement avec un *Ctrl+C* ou un *SIGTERM*.

Les options *REFRESH_DELAY* et *RANDOM_DELAY* permettent de spécifier des temps en seconde (nombre entier uniquement).

Les constantes en début de fichier pourront être éditées pour adapter le comportement du script.
Notamment pour modifier le chemin du fichier de journalisation JSON, la verbosité, ou les délais d'actualisation par défaut.


### 3.1. Alias

On pourra se définir des *alias* au besoin pour l'ergonomie.

```bash
$ nano .bash_aliases
[...]
alias logipchanges='cd /home/<path>/bash-public-ip-logger/ ; bash script--log-public-ips-changes.sh'
alias logipchanges_fastrefresh='cd /home/<path>/bash-public-ip-logger/ ; bash script--log-public-ips-changes.sh 10 5'
alias logipchanges_slowrefresh='cd /home/<path>/bash-public-ip-logger/ ; bash script--log-public-ips-changes.sh 900 300'
```
 

### 3.2. Exemple

Intervalle d'actualisation de 5 min avec un supplément aléatoire de 2 min.

Illustration par un changement de l'interface de connexion maître de la machine
(wifi1 → déco/reco → wifi2 → interruption).

```bash
$ logipchanges 
2021-02-01 12:14:03:	starting... Refresh delay on 300 + [0;120] seconds.
2021-02-01 12:14:03:	92.123.123.35
2021-02-01 12:19:20:	NoInternet
2021-02-01 12:25:36:	37.123.123.13
2021-02-01 12:37:11:	NoInternet
2021-02-01 12:42:49:	92.123.123.35
^C
```

Exemple de sortie *JSON* :

```bash
$ cat log--Public-IPs-Changes--2021-02.json 
{"ip": "92.123.123.35", "timestamp": 1612178043, "date": "2021-02-01"}
{"ip": "NoInternet", "timestamp": 1612178360, "date": "2021-02-01"}
{"ip": "37.123.123.13", "timestamp": 1612178736, "date": "2021-02-01"}
{"ip": "NoInternet", "timestamp": 1612179431, "date": "2021-02-01"}
{"ip": "92.123.123.35", "timestamp": 1612179769, "date": "2021-02-01"}
```

### 3.3. Bonus

Voici d'autres alias sympathiques, qui tiennent en une ligne :

**Quel est mon IP publique ?**
[HTML](https://framagit.org/Grapha/bash-public-ip-logger/-/snippets/6146), 
[RAW](https://framagit.org/Grapha/bash-public-ip-logger/-/snippets/6146/raw/main/whatismyip.sh)

**A quel pays une IP apartient ?**
[HTML](https://framagit.org/Grapha/bash-public-ip-logger/-/snippets/6147), 
[RAW](https://framagit.org/Grapha/bash-public-ip-logger/-/snippets/6147/raw/main/geoip_countrycode.sh)

**Quels informations sont rattachées à une IP ?**
[HTML](https://framagit.org/Grapha/bash-public-ip-logger/-/snippets/6148), 
[RAW](https://framagit.org/Grapha/bash-public-ip-logger/-/snippets/6148/raw/main/geoip.sh)

**Un résumé par l'exemple :**

```bash
$ whatismyip
92.123.123.35
$ dig +short google.com | geoip
$ geoip 172.217.168.46alias logipchanges_fastrefresh='cd path/to/bash-public-ip-logger/ ; bash script--log-public-ips-changes.sh 10 5'
alias logipchanges_slowrefresh='cd path/to/bash-public-ip-logger/ ; bash script--log-public-ips-changes.sh 900 300'
{
  "ip": "172.217.168.46",
  "hostname": "zrh04s14-in-f14.1e100.net",
  "city": "Mountain View",
  "region": "California",
  "country": "US",
  "loc": "37.3860,-122.0838",
  "org": "AS15169 Google LLC",
  "postal": "94035",
  "timezone": "America/Los_Angeles",
  "readme": "https://ipinfo.io/missingauth"
}
$ geoip_countrycode 172.217.168.46
US
```

**Et pour les utilisateurs de Windows ?**

 - La commande équivalente à **dig** est **[Resolve-DnsName](https://docs.microsoft.com/en-us/powershell/module/dnsclient/resolve-dnsname)**.
 - La commande équivalente à **curl** est **[Invoke-WebRequest](https://technet.microsoft.com/en-us/library/hh849901.aspx)**.

Voici une piste pour faire un script *.bat* équivalent.
([HTML](https://framagit.org/Grapha/bash-public-ip-logger/-/snippets/6298), 
[RAW](https://framagit.org/Grapha/bash-public-ip-logger/-/snippets/6298/raw/main/whatismyip.sh),
[source](https://stackoverflow.com/questions/54175952/dig-like-functionality-on-windows)).


---


## 4. Documentation

La documentation est fournie dans ce fichier, et par quelques commentaires dans le script.



---

**Auteur :** Grapha *grapha.at.netc.fr*

**Date de modification :** 13-09-2023

---

« Knowledge must be shared freely. That's sharing power, what defines humanity. »
