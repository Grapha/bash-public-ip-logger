#!/bin/bash
# encoding: utf-8

# ==============================================================================
#
# This file is part of BashPublicIPLogger.
# Copyright 2020-2023, Jonathan Lamont (alias Grapha).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ==============================================================================
#
# Tools that continuously shows and logs each change of the current public IP.
#
# Depends on command 'python3'.
#


## Debug:
#set -xe


## Constants:
QUIET=0														# set to 1 to avoid display on stdout
VERBOSE=0													# set to 0 to only show when changed, to 1 for each change
REFRESH_DELAY=180											# seconds (default minimum)
RANDOM_DELAY=120											# seconds (in addition to minimum)
OUT_FP="log--Public-IPs-Changes--$(date +'%Y-%m').json"		# JSON output log file
MY_IP=""
MY_OLD_IP=""


## Functions:

function print_usage {
	echo "Bash - Public IP Changes Logger"
	echo ""
	echo "Usage:"
	echo "script--log-public-ips-changes.sh [REFRESH_DELAY [RANDOM_DELAY]]"
}

function print_help {
	print_usage
	echo ""
	echo "Description:"
	echo -e "\tTools that continuously shows and logs each change of the current public IP."
	echo -e "\t"
	echo -e "\tRelies on Internet via 'ifconfig.me' to get the public IP."
	echo -e "\tAlso logs each IP changes into a JSON file."
	echo -e "\t"
	echo -e "\tOptions REFRESH_DELAY and RANDOM_DELAY must be provided as integer of seconds."
	echo -e "\tOptionally, you may adapt the constants defined on top of this script to fit the behavior you want."
	echo ""
	echo "License:"
	echo "GNU GPLv3 or latter"
}

function abort_on_not_a_integer {
	# takes one arg that must be an int, otherwise exit on error
	re='^[0-9]+$'
	if ! [[ $1 =~ $re ]]; then
		echo "[-] Error: Not an integer." >&2
		exit 1
	fi
}

function ctrl_c_exit {
	log_ip_change "$OUT_FP" "$MY_IP"
	echo -e "$(date +'%F %T'):\tEnding... Logs updated."
	exit 0
}

function get_new_rand_delay {
	local refresh_delay random_delay
	refresh_delay="$1"
	random_delay="$2"
	[ -z "$refresh_delay" ] && refresh_delay=180
	[ -z "$random_delay" ] && random_delay=120
	python3 -c 'import random, sys; print(int(sys.argv[1]) + random.randint(0, (int(sys.argv[2])) if(len(sys.argv) > 2) else 120))' "$refresh_delay" "$random_delay"
}

function get_current_ip {
#dig +short myip.opendns.com @resolver1.opendns.com 2>/dev/null
#curl ifconfig.me 2>/dev/null
	local new_ip
	new_ip=$(curl ifconfig.me 2>/dev/null)
	if [ -z "$new_ip" ] || [ "$new_ip" = "<html></html>" ] || [ $(echo "$new_ip" | xargs | grep -Evc '^([0-9]{1,3}\.){3}[0-9]{1,3}$') -ne 0 ]; then
		echo "NoInternet"
	else
		echo "$new_ip"
	fi
}

function display_change {
	echo -e "$(date +'%F %T'):\t${MY_IP}"
}

function log_ip_change {
	local fp ip
	fp="$1"
	ip="$2"
	[ -z "$fp" ] && return 1
	echo -e "{\"ip\": \"${ip}\", \"timestamp\": $(date +%s), \"date\": \"$(date +%F)}\"" >> "$fp"
}


## [Main Script]:

### Checks arguments:
if [ "$1" = "--help" ] || [ "$1" = "-h" ]; then
	print_help
	exit 0
fi
if [ $# -gt 2 ]; then
	echo "[-] Wrong arguments."
	print_usage
	exit 1
fi
if [ $# -ge 1 ]; then
	abort_on_not_a_integer "$1"
	REFRESH_DELAY="$1"
fi
if [ $# -ge 2 ]; then
	abort_on_not_a_integer "$2"
	RANDOM_DELAY="$2"
fi
if [ $QUIET -eq 0 ]; then
	echo -e "$(date +'%F %T'):\tStarting... Refresh delay on $REFRESH_DELAY + [0;${RANDOM_DELAY}] seconds."
fi
MY_IP=$(get_current_ip)
log_ip_change "$OUT_FP" "$MY_IP"
[ $QUIET -eq 0 ] && display_change
trap ctrl_c_exit INT										# trap ctrl-c interruption and call ctrl_c()
while true;
do
	sleep $(get_new_rand_delay "$REFRESH_DELAY" "$RANDOM_DELAY")

	MY_OLD_IP="$MY_IP"
	MY_IP=$(get_current_ip)
	if [ "$MY_IP" != "$MY_OLD_IP" ]; then
		[ $QUIET -eq 0 ] && display_change
		log_ip_change "$OUT_FP" "$MY_IP"
		# TODO possibility to mail the IP change
		# TODO possibility to push the new IP as desktop notification
	else
		[ $VERBOSE -eq 1 ] && display_change
	fi
done


